<?php

/**
 * Задача 6. Реализовать вход администратора с использованием
 * HTTP-авторизации для просмотра и удаления результатов.
 pass:1123;
 **/

// Пример HTTP-аутентификации.
// PHP хранит логин и пароль в суперглобальном массиве $_SERVER.
// Подробнее см. стр. 26 и 99 в учебном пособии Веб-программирование и веб-сервисы.
$user = 'u20964';
$pass = '6677669';
$db = new PDO('mysql:host=localhost;dbname=u20964', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try{
  $stmt = $db->prepare("SELECT * FROM `admin`");
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_NUM);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
}
if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != $result[0] ||
    md5($_SERVER['PHP_AUTH_PW']) != $result[1]) { 
  header('HTTP/1.1 401 Unanthorized');
  header('WWW-Authenticate: Basic realm="My site"');
  print('<h1>401 Требуется авторизация</h1>');
  exit();
}

print('Вы успешно авторизовались и видите защищенные паролем данные.');


// *********
// Здесь нужно прочитать отправленные ранее пользователями данные и вывести в таблицу.
// Реализовать просмотр и удаление всех данных.
// *********
?>

<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Admin</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script
      src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
</body>
</html>
<body>
	<table>
		<?php
			for ($i=0; $i < 3; $i++) {
				print("<tr>");
					for ($j=0; $j < 3; $j++) { 
						print("<td> $j </td>");
					}
				print("</tr>");
			}
		?>
	</table>

	<table>
		<tr>
			<td>Бессмертных</td>
			<td>Проходящих сквозь стены</td>
			<td>Левитирующих</td>
		</tr>
		<tr>
			<?php
			$user = 'u20964';
			$pass = '6677669';
			$db = new PDO('mysql:host=localhost;dbname=u20964', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
			try{
			  for ($i=1; $i <= 3 ; $i++) { 
				$stmt = $db->prepare("SELECT COUNT(*) FROM `super_power` WHERE `sp_id` = ?");
			    $stmt->execute(array($i));
			    $result = $stmt->fetch(PDO::FETCH_NUM);
			    print("<td>" . $result[0] . "</td>");
			  }
			}
			catch(PDOException $e){
			  print('Error : ' . $e->getMessage());
			}
			?>
		</tr>
	</table>
</body>
